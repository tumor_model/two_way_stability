% This program confirms that there is at least roughly similar behavior in
% different sections of the 2-way heat plot.

% Enter in desired values of c and a
c = 0.04; % Range for c is 0<c<0.05
tumor_p.a = 1.5; % Range for a is 0<a<2

% Used (0.01, 0.5), (0.02, 1), (0.04, 1.5). Vera 3/23/17

% Fixed parameters from Table 1 on page 138.
tumor_p.mu_2 = 0.03;
tumor_p.p_1 = 0.1245;
tumor_p.g_1 = 20000000;
tumor_p.g_2 = 100000;
tumor_p.r_2 = 0.18;
tumor_p.b = 0.000000001;
tumor_p.mu_3 = 10;
tumor_p.p_2 = 5;
tumor_p.g_3 = 1000;

% Add s_1 and s_2 to the tumor parameters. These are zero for now.
tumor_p.s_1 = 0;
tumor_p.s_2 = 0;

y0 = [1, 1, 1]; % Set initial values (x_0, y_0, z_0). Anything small and non-zero works

% Get the plot for c = 0.035
tspan = [0, 1000];

% Generate 2D and 3D plots
options = odeset('OutputFcn',@odephas3);
[t,y] = ode23s(@(t, y) ODE_system(t, y, c, tumor_p), tspan, y0, options);
c_label = num2str(c, '%.2f');
a_label = num2str(tumor_p.a, '%.2f');
title_string = ['c = ' c_label ', a = ' a_label];
title(title_string)
xlabel('Effector Cells (x)')
ylabel('Tumor Cells (y)')
zlabel('IL-2 (z)')

fig2 = figure;
plot(t, y)
title(title_string)
xlabel('Time in Days')
ylabel('Volume')
legend('Effector Cells', 'Tumor Cells', 'IL-2')
