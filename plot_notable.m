% This program assumes that you ran two_way_stability.m, and saved the
% ac_array and notable_values. These should be loaded in the
% workspace. This program will go through those structs and make vectors
% for plotting.

% This program is modified from bifurcation_digram, which considers c.
% In this version, I am plotting just the values that got identified as
% "notable". Vera 3/6/17.

a_points = nan(1, length(notable_values));
c_points = nan(1, length(notable_values));

for i = 1:length(notable_values)
    value_struct = notable_values{i};
    a_points(i) = value_struct.a_value;
    c_points(i) = value_struct.c_value;
end

fig1 = figure;
plot(c_points, a_points, 'p')
ylim([0, 2])
title('Notable Values')
ylabel('Immune Response (a)')
xlabel('Tumor Agenticity (c)')
        
    