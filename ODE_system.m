function dydt = ODE_system( t, y , c, tumor_p)
% This implements the equations given as numbers 6, 7, and 8 on page 241 of
% Kirschner and Panetta (1998).

% CALLED BY:
%   four_plots_2D
%   four_plots_3D

% The variable matrix has to be named y, even though we actually have the
% variables (x, y, z)

% x = y(1,:)
% y = y(2,:)
% z = y(3,:)

% Get values from tumor_p struct
mu_2 = tumor_p.mu_2;
p_1 = tumor_p.p_1;
g_1 = tumor_p.g_1;
g_2 = tumor_p.g_2;
r_2 = tumor_p.r_2;
b = tumor_p.b;
a = tumor_p.a;
mu_3 = tumor_p.mu_3;
p_2 = tumor_p.p_2;
g_3 = tumor_p.g_3;

% s_1 and s_2 represent treatments, at this stage we are not looking at
% any treatments so these are both zeros
s_1 = tumor_p.s_1;
s_2 = tumor_p.s_2;

% Preallocate
dydt = y;

dydt(1,:) = c * y(2,:) - mu_2 * y(1,:) + p_1 * y(1,:) .* y(3,:) ./ (g_1 + y(3,:)) + s_1;
dydt(2,:) = r_2 * y(2,:) .* (1 - b * y(2,:)) - a * y(1,:) .* y(2,:) ./ (g_2 + y(2,:));
dydt(3,:) = p_2 * y(1,:) .* y(2,:) / (g_3 + y(2,:)) - mu_3 * y(3,:) + s_2;

end

