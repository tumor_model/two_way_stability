% This program assumes that you ran two_way_stability.m, and saved the
% ac_array and notable_values. My goal here is to use a simple formula to
% convert the informatoin saved in ac array into values in a matrix that I
% can use to make a heat map. Vera 3/6/17.

[num_a_values, num_c_values] = size(ac_array);

% Preallocate
ac_matrix = nan(num_a_values, num_c_values);

for i = 1:num_a_values

    for j = 1:num_c_values
        value_struct = ac_array{i, j};
        num_equilibria = value_struct.num_relevant_equilibria;
        % Note this is only the relevant equilibria
        
        num_imaginary = 0;
        num_negative = 0;
        num_zero = 0;
        
        for k = 1:num_equilibria
            num_imaginary = num_imaginary + value_struct.imaginary_eigenvalues{k};
            num_negative = num_negative + value_struct.negative_eigenvalues{k};
            num_zero = num_zero + value_struct.zero_eigenvalues{k};
        end
        
        % This is a sum of all the "determining" factors with random
        % weights I made up. The goal is that each "type" of equilibrium
        % gets a different sum.
        representative_sum = num_equilibria + 19 * num_imaginary +  27 * num_negative + 13 * num_zero;
        
        ac_matrix(i, j) = representative_sum;
    end
end

% Make a heat map using the matrix
colormap('hot')
imagesc(ac_matrix)
xlim([0, 51])
ylim([0, 41])
title('Two Way Stability')
xlabel('Tumor Agenticity (c)')
ylabel('Immune Response (a)')
set(gca, 'Ydir', 'normal')

[rows, columns] = size(ac_matrix);

% Make useful axis labels. Vera 3/23/17
a_range = 0:0.5:2;
num_y_ticks = length(a_range);
y_step = rows / (num_y_ticks - 1);
y_tick_count = 0:y_step:rows;
y_ticks = {1, num_y_ticks};
for n = 1:num_y_ticks
    y_ticks{n} = num2str(a_range(n));
end

set(gca, 'YTick', y_tick_count)
set(gca, 'YTickLabel', y_ticks)

c_range = 0:0.01:0.05;
num_x_ticks = length(c_range);
x_step = columns / (num_x_ticks - 1);
x_tick_count = 0:x_step:columns;
x_ticks = {1, num_x_ticks};
for m = 1:num_x_ticks
    x_ticks{m} = num2str(c_range(m));
end

set(gca, 'XTick', x_tick_count)
set(gca, 'XTickLabel', x_ticks)
